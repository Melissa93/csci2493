import java.util;
import java.io;


class Stack {
	 public static void main (String[] args){
	Stack stack1 = new Stack();
	
	Stack<String> stack2 = new Stack<String>();

	stack1.push(4);
	stack1.push("Elephant");
	stack1.push("Lizard");

	stack2.push("Dog");
	stack2.push("Cat");
	stack2.push("Lion");

	System.out.println(stack1);
	System.out.println(stack2);
	}
}

