class interpolationSearch
{
    public static int interpolationSearch(int[] A, int x)
    {
        int n = 0;
        int y = A.length - 1;
 
        while (A[y] != A[n] && x >= A[n] && x <= A[y])
        {
            int mid = n + ((x - A[n]) * (y - n) /
                            (A[y] - A[n]));
 
            if (x == A[mid]) {
                return mid;
            }
 
            else if (x < A[mid]) {
                y = mid - 1;
            }
 
            else {
                n = mid + 1;
            }
        }
 
        if (x == A[n]) {
            return n;
        }
 
        return -1;
    }
 
    public static void main(String[] args)
    {
        int[] A = {2, 5, 6, 8, 9, 10};
        int key = 5;
 
        int index = interpolationSearch(A, key);
 
        if (index != -1) {
            System.out.println("Element found at index " + index);
        }
        else {
            System.out.println("Element not found in the array");
        }
    }
}
