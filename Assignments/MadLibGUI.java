import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class MadLibGUI implements ActionListener {

	JFrame wmlGame = new JFrame("Mad Lib GUI");
	JPanel gameBoard = new JPanel(new GridLayout(3,3));

	JLabel place1Label = new JLabel("place");
	JLabel noun1Label = new JLabel("noun");
	JLabel animal1Label = new JLabel("animal");

	JLabel verb1 = new JLabel("verb ");
	JLabel pluralverb1 = new JLabel("plural verb ");
	JLabel villian1 = new JLabel("villain ");
	JLabel adjective1 = new JLabel("adjective ");

	JTextField verb = new JTextField(25);
	//String[] objectOptions = { " a ball."," chickens."," a kazoo.", " an igloo." };
	//JComboBox object = new JComboBox(objectOptions);

	JButton clear = new JButton("Clear");
	JTextArea sentence = new JTextArea("Ron Stoppable was on a mission with Rufus at" + place.getText() + "when a" + animal.getText() + "came out of thin air and"  + verb.getText() + "Ron. He flys across the room, when his pants yet again fall down. Ahh not again! he screams" + villain.getText() + "appears, and Rufus charges at them. Rufus" + adjective.getText() +
	"missionsuit saves them in the end by giving him powers of the monkey king. Boo-Yah!");


	JButton exit = new JButton("Exit");

	public MadLibGUI() {

		wmlGame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		wmlGame.setSize(1000,1000);

		subjectLabel.setOpaque(true);
		subjectLabel.setBackground(new Color(0,255,255));
		subjectLabel.setHorizontalAlignment(JLabel.CENTER);

		sentence.setEditable(false);
		sentence.setOpaque(false);
		sentence.setLineWrap(true);
		sentence.setWrapStyleWord(true);

		exit.addActionListener(this);
		clear.addActionListener(this);
		verb.addActionListener(this);
		object.addActionListener(this);

		gameBoard.add(placeLabel);
		gameBoard.add(nounLabel);
		gameBoard.add(animalLabel);
		gameBoard.add(verb);
		gameBoard.add(pluralverb);
		gameBoard.add(villian);
		gameBoard.add(adjective);

		gameBoard.add(clear);
		gameBoard.add(sentence);
		gameBoard.add(exit);


		wmlGame.setContentPane(gameBoard);
		wmlGame.setVisible(true);
		wmlGame.pack();
	}

	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == exit) {

			wmlGame.dispose();

		} else if (e.getSource() == clear) {

			sentence.setText("");

		} else if ((e.getSource() == noun) || (e.getSource() == animal)) {

			String combinedWords = place.getText() + noun.getText() + animal.getText() + verb.getText() + plural.getText() + villian.getText() + adjective.getText();
			sentence.setText(combinedWords);

		}

	}

	public static void main(String[] args) {

		new MadLibGUI();

	}

}

