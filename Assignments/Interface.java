interface Puma {
	public void Method1(); //interface method one
}

interface Lion {
	public void Method2(); //2nd interface method
}

class Feline implements Puma, Lion {
	public void Method1() {
		System.out.println("Puma's eat a range of small prey: Squirrels,rabbits ets"); //method one explains that for the puma there has to be rabbits etc to survive. 
	}
	public void Method2() {
		System.out.println("Lion's eat more bigger prey like zebras, giraffe. "); //method two explains that for a lion to survive there needs to be an abundant amount of these prey for them to hunt. 
	}
}

 
//an interface provides a computer to enforce certain properties on an object or class. 
// with an interface the programmer can specify how you want methods and fields. 
//https://www.w3schools.com/java/java_interface.asp this was my reference for the code. 
