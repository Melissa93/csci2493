public class RecursionExample{
static void method(){
System.out.println("WOA! Recursion");
method();
}

public static void main(String[] args){
method();
}
}

//Recursion is like a forever loop. It continuously calls to itself. Which in my example is 
//"WOA! Recursion" it can also be a counting number. You state what you want it to call itself over and over and then state a method. 
//you have to call the method to be executed. And calling it again at the end makes the recursion. 
//you have to have that main statment of public static void or it won't work.Defining the main method as public.  
